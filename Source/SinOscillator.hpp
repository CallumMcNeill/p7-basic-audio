//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Callum McNeill on 19/11/2017.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


class SinOscillator
{
public:
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    //sets the value of sample rate
    void setSampleRate(int newSampleRate);
    
    //returns the next sine wave sample
    float getSample();
    
    //updates the oscillator frequency
    void setFrequency(float newFrequency);
    
    //updates the oscillator amplitude
    void setAmplitude(float newAmplitude);
    
    //returns the amplitude
    float getAmplitude();
    
    
private:
    float frequency = 440.f;
    float phasePosition = 0.f;
    float sampleRate;
    float amplitude = 0.5f;
};

#endif /* SinOscillator_hpp */
