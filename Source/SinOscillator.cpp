//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Callum McNeill on 19/11/2017.
//
//

#include "SinOscillator.hpp"

SinOscillator::SinOscillator()
{
    
}

SinOscillator::~SinOscillator()
{
    
}

void SinOscillator::setSampleRate(int newSampleRate)
{
    sampleRate = newSampleRate;
}

float SinOscillator::getSample()
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency)/sampleRate;
    
    phasePosition += phaseIncrement;
    
    if (phasePosition > twoPi)
    {
        phasePosition -= twoPi;
    }
    
    const float sinValue = sin(phasePosition);
    
    return sinValue;
}

void SinOscillator::setFrequency(float newFrequency)
{
    frequency = newFrequency;
}

void SinOscillator::setAmplitude(float newAmplitude)
{
    amplitude = newAmplitude;
}

float SinOscillator::getAmplitude()
{
    return amplitude;
}

