/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) :   Thread ("CounterThread"),
                                            audio (a)
{
    setSize (500, 400);
    
    startThread();
    
    addAndMakeVisible(textButton1);
    textButton1.setButtonText("Start/Stop Counter");
    textButton1.addListener (this);
}

MainComponent::~MainComponent()
{
    stopThread(500);
}

void MainComponent::resized()
{
    textButton1.setBounds(20, 20, getWidth(), 40);
}

void MainComponent::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        std::cout << "Counter:" << Time::getApproximateMillisecondCounter() << "\n";
        Time::waitForMillisecondCounter (time + 100);
    }
    
    
}

void MainComponent::buttonClicked (Button* button)
{
    
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

